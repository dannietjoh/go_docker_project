FROM golang:latest as build
WORKDIR /go/src/gitlab.com/dannietjoh/go_docker_project
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine:latest
RUN adduser -D -g '' gouser
COPY --from=build /go/src/gitlab.com/dannietjoh/go_docker_project/go_docker_project /
USER gouser
ENTRYPOINT ["/go_docker_project"]
