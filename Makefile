.PHONY: test lint build tag push deploy up down restart obliterate reload pull logs cloc help
.DEFAULT_GOAL := help

BUILD_NAME := go_docker_project
BUILD_TAG := $(shell git describe --always --tags --abbrev=0)
DOCKER_REGISTRY :=
GIT_GROUP := dannietjoh
GIT_REPO := go_docker_project
GIT_SERVER := gitlab.com
OS := $(shell uname)

ifeq ($(OS), Darwin)
	BROWSER_OPEN := open
else
ifeq ($(OS), Linux)
	BROWSER_OPEN := xdg-open
else
ifeq ($(OS), Windows_NT)
	BROWSER_OPEN := explorer
endif
endif
endif

##@ Build tasks
dep: ## Update package dependencies
	@dep ensure

test: ## Test code
	docker run -v ${PWD}:/go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO}/${BUILD_NAME} -w /go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO}/${BUILD_NAME} ${DOCKER_REGISTRY}golang:latest go test -v .
	${MAKE} clean-docker

lint: ## Run linters
	docker run -v ${PWD}/Dockerfile:/Dockerfile:ro redcoolbeans/dockerlint -p
	docker run -v ${PWD}:/go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO}/${BUILD_NAME} -w /go/src/${GIT_SERVER}/${GIT_GROUP}/${PROJECGIT_REPOT_NAME}/${BUILD_NAME} leveldorado/metalinter:latest gometalinter . --deadline=120s
	${MAKE} clean-docker

build: ## Build docker container
	docker build . -t ${BUILD_NAME}

tag: ## Tag containers
	docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:latest

push: ## Push containers to registry
	docker login -u ${DOCKER_REPO_USER} -p ${DOCKER_REPO_TOKEN} https://${DOCKER_REGISTRY}
	docker push ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	docker push ${DOCKER_REGISTRY}${BUILD_NAME}:latest

deploy: ## Deploy
	docker-compose up -d

clean-docker: ## Clean exited docker containers
	@echo removing exited docker containers...
	@docker rm $$(docker ps -qa --no-trunc --filter "status=exited") || true

all: ## Full test lint build-docker deploy cycle
	@${MAKE} test lint build-docker deploy

##@ Service management containers
up: ## Bring docker containers up
	docker-compose up -d

down: ## Bring docker containers down
	docker-compose down

restart: ## Restart docker containers
	@${MAKE} down up

obliterate: ## Destroy docker containers and volumes
	docker-compose down -v

reload: ## Restart docker containers
	docker-compose restart

pull: ## Pull docker containers
	docker-compose pull

logs: ## Show docker container logs
	docker-compose logs -f

top: ## Show docker container processes
	docker-compose top

##@ Helpers
cloc: ## Show Lines of Code analysis
	@cloc --vcs git --quiet

doc: ## Show go documentation
	godoc -http :8000 &
	$(BROWSER_OPEN) http://localhost:8000

help: ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
