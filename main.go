package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// User -
type User struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Email    string `json:"email,omitempty"`
}

// UserDB -
type UserDB struct {
	Users []User `json:"users,omitempty"`
	Type  string `json:"type,omitempty"`
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func createDB(dbfile string) {
	if _, err := os.Stat(dbfile); os.IsNotExist(err) {
		fmt.Println("Creating new database", dbfile)
		_, err := os.Create(dbfile)
		checkError(err)
	}
	fmt.Println("Using existing database", dbfile)
}

func readDB(dbfile string) (data []byte) {
	data, err := ioutil.ReadFile(dbfile)
	checkError(err)
	return
}

func writeDB(dbfile string, data []byte) {
	err := ioutil.WriteFile(dbfile, data, 0640)
	checkError(err)
}

// jsonMarshal - returns the JSON encoding value of v as []byte
func jsonMarshal(input interface{}) (output []byte) {
	fmt.Println("marshaling json...")
	output, err := json.Marshal(input)
	checkError(err)
	return
}

// jsonUnmarshal - parses JSON-encoded data and stores it as interface{}
func jsonUnmarshal(input []byte, output interface{}) interface{} {
	fmt.Println("unmarshaling json...")
	err := json.Unmarshal(input, &output)
	checkError(err)
	return output
}

func main() {
	// vars
	decodedJSON := UserDB{}
	group := "testgroup"
	readdbfile := "read" + group + ".json"
	writedbfile := "write" + group + ".json"

	// create db
	createDB(readdbfile)

	// read db
	data := readDB(readdbfile)

	// decode data
	jsonUnmarshal(data, &decodedJSON)

	// modify data
	newuser := User{Username: "New User", Password: "14351", Email: "new@user.com"}
	fmt.Println("new user:", newuser)
	decodedJSON.Users = append(decodedJSON.Users, newuser)

	// encode json
	encodedJSON := jsonMarshal(decodedJSON)
	// write to database
	writeDB(writedbfile, encodedJSON)

}
